import unittest
import LogUtil
from LogUtil import EditRec


class TestStringMethods(unittest.TestCase):

    def test_merge_insert_and_insert(self):
        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbb',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            }
        ]
        # merge only one insert => one insert
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='bbb', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123456))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbb',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'bbbccc',
                'changeText': '123',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123457'
            }
        ]
        # merge two inserts => one insert
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='123bbb', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123457))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbb',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaabbb',
                'changeTextEndAfterText': 'ccc',
                'changeText': '234',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123457'
            },
            {
                'changeTextStartBeforeText': 'aaabb',
                'changeTextEndAfterText': 'b234ccc',
                'changeText': '567',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123460'
            }
        ]
        # merge three inserts => one insert
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='bb567b234', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123460))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbb',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaabbb',
                'changeTextEndAfterText': 'ccc',
                'changeText': '234',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123457'
            },
            {
                'changeTextStartBeforeText': 'aaabbb234cccfghj',
                'changeTextEndAfterText': 'uuuu',
                'changeText': '567',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123460'
            }
        ]
        # merge three inserts => 1,2 merged, 3 not merge
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 2)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='bbb234', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123457))
        self.assertEqual(merges[1], EditRec(before='aaabbb234cccfghj', after='uuuu', change='567', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123460))

    def test_merge_insert_and_delete(self):
        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbb',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbb',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123457'
            }
        ]
        # delete == insert, vanish two edits
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 0)

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbbdddlll',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaabbb',
                'changeTextEndAfterText': 'lllccc',
                'changeText': 'ddd',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete is inside insert, merge to one insert
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='bbblll', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbbdddlll',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'dlllccc',
                'changeText': 'bbbdd',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete inside with left bound, merge to one insert
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='dlll', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbbdddlll',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaabb',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bdddlll',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete inside with right bound, merge to one insert
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='bb', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbbdddlll',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'a',
                'changeTextEndAfterText': 'dddlllccc',
                'changeText': 'aabbb',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete overlap with insert left, merge to one insert and one delete
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 2)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='dddlll', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123456))
        self.assertEqual(merges[1], EditRec(before='a', after='dddlllccc', change='aa', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'bbbdddlll',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaabbbd',
                'changeTextEndAfterText': 'cc',
                'changeText': 'ddlllc',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete overlap with insert right, merge to one insert and one delete
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 2)
        self.assertEqual(merges[0], EditRec(before='aaa', after='ccc', change='bbbd', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123456))
        self.assertEqual(merges[1], EditRec(before='aaabbbd', after='cc', change='c', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'hijklll',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aa',
                'changeTextEndAfterText': 'cc',
                'changeText': 'ahijklllc',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete cover insert, merge to one delete
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aa', after='cc', change='ac', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'cccttttt',
                'changeText': 'lll',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaalllcc',
                'changeTextEndAfterText': 'ttt',
                'changeText': 'ctt',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # insert no cross delete, no merge
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 2)
        self.assertEqual(merges[0], EditRec(before='aaa', after='cccttttt', change='lll', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123456))
        self.assertEqual(merges[1], EditRec(before='aaalllcc', after='ttt', change='ctt', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123462))

    def test_merge_delete_and_delete(self):
        recs = [
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'bbb',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'ccc',
                'changeText': 'ccc',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete_2 after delete_1, merge to one delete
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaaaaa', after='ccc', change='bbbccc', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'bbb',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'aaa',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete_2 before delete_1, merge to one delete
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 1)
        self.assertEqual(merges[0], EditRec(before='aaa', after='cccccc', change='aaabbb', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123462))

        recs = [
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'bbb',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaaaaacc',
                'changeTextEndAfterText': 'c',
                'changeText': 'ccc',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete_1 no cross delete_2, no merge
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 2)
        self.assertEqual(merges[0], EditRec(before='aaaaaa', after='cccccc', change='bbb', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123456))
        self.assertEqual(merges[1], EditRec(before='aaaaaacc', after='c', change='ccc', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123462))

    def test_merge_delete_and_insert(self):
        recs = [
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'bbb',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'bbb',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete == insert, vanish two edits
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 0)

        recs = [
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'bbb',
                'changeType': 0,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123456'
            },
            {
                'changeTextStartBeforeText': 'aaaaaa',
                'changeTextEndAfterText': 'cccccc',
                'changeText': 'fff',
                'changeType': 1,
                'filename': 'example_1.java',
                'uuid': "123456",
                'createTime': '123462'
            }
        ]
        # delete != insert, no merge
        merges = LogUtil.merge_one_line_records(recs)
        self.assertEqual(len(merges), 2)
        self.assertEqual(merges[0], EditRec(before='aaaaaa', after='cccccc', change='bbb', chg_type=0,
                                            f_name='example_1.java', uuid='123456', time=123456))
        self.assertEqual(merges[1], EditRec(before='aaaaaa', after='cccccc', change='fff', chg_type=1,
                                            f_name='example_1.java', uuid='123456', time=123462))


if __name__ == '__main__':
    unittest.main()
