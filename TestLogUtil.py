import json
import LogUtil


log_path = "C:\\Users\\liut\\DATA\\server_2023.2.10_-2023-03-23T02-45-33.216.log.gz"


def check_one_line():
    samples = LogUtil.get_head(log_path, 10)
    ss = samples[0]
    for ll in ss:
        jstr = json.dumps(ll, indent=4, ensure_ascii=False)
        print(jstr)


def test_extract_examples(start_index, count):
    samples_folder = "C:\\Users\\liut\\OUTPUT\\samples"
    LogUtil.extract_examples(log_path, start_index, count, samples_folder)


def test_merge_examples(start_index, count):
    samples_folder = "C:\\Users\\liut\\OUTPUT\\merged_samples"
    LogUtil.merge_examples(log_path, start_index, count, samples_folder)


def test_diff_examples(start_index, count):
    samples_folder = "C:\\Users\\liut\\OUTPUT\\compare_samples"
    LogUtil.diff_examples(log_path, start_index, count, samples_folder)


# check_one_line()
# test_extract_examples(20, 100)
# test_merge_examples(20, 30)
# test_diff_examples(0, 10)

LogUtil.merge_gz_log(log_path, "C:\\Users\\liut\\OUTPUT\\merged_data")
