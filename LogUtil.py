from collections import deque
from collections import namedtuple
import gzip
import json
import os


def get_line_count(path):
    count = 0
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            if count <= 100:
                print(line)
            count += 1
        if count % 1000 == 0:
            print(count)
    return count


def get_head(path, count):
    n = 0
    ret = []
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            n += 1
            if n > count:
                break
            ss = line.split("写入日志：")[1].strip()
            dd = eval(ss)
            dd = eval(dd['json:'])
            ret.append(dd)
    return ret


def get_range(path, begin, count):
    assert begin >= 0
    n = -1
    ret = []
    with open(path, 'r', encoding='utf-8') as f:
        for line in f:
            n += 1
            if n < begin:
                continue
            if n > count-1:
                break
            ss = line.split("写入日志：")[1].strip()
            dd = eval(ss)
            dd = eval(dd['json:'])
            ret.append(dd)
    return ret


def extract_examples(path, begin, count, save_folder):
    assert begin >= 0
    n = -1
    with gzip.open(path, 'rb') as f:
        for line in f:
            line = line.decode()
            n += 1
            if n < begin:
                continue
            if n > count-1:
                break
            ss = line.split("写入日志：")[1].strip()
            dd = eval(ss)
            dd = eval(dd['json:'])
            with open(os.path.join(save_folder, f"example_{n}.json"), 'w', encoding='utf-8') as wf:
                for ll in dd:
                    jstr = json.dumps(ll, indent=4, ensure_ascii=False)
                    wf.write(jstr)
                    wf.write('\n')


EditRec = namedtuple('EditRec', ['before', 'after', 'change', 'chg_type', 'f_name', 'uuid', 'time'])
EditStep2 = namedtuple('EditStep2', ['org_before', 'org_after', 'change', 'chg_type', 'org_time', 'can_merge', 'before', 'after', 'time'])


def _error_check_and_fix(pre_text, before, change, chg_type, after):
    cur_text = before + after if chg_type == 1 else before + change + after
    if pre_text == cur_text:  # no error
        return before, change, after
    if chg_type == 1:
        offset = len(before)
        if pre_text[:offset] == before:
            # case 1: "\n\n    publ" => "\n\n    "
            chg_tail = change.strip()
            if pre_text[offset:offset+10] != after[:10]:
                fix_d = (chg_tail + after[:10]).strip()
                mod_head = pre_text[offset:offset+len(fix_d)+10].strip()
                if fix_d == mod_head[:len(fix_d)]:  # fix successfully
                    change = change[:-len(chg_tail)]
                    after = chg_tail + after
                    return before, change, after
                else:
                    # print(f"Not fixed of Case 1, change:{change[:10]}, after:{after[:10]}")
                    return before, change, after
            else:
                # print("Other errors detected")
                return before, change, after
        else:
            # print("Not supported fix yet")
            return before, change, after
    else:  # cannot fix delete edit
        return before, change, after


def error_check_and_fix(stack):
    fix_stack = []
    for rec in stack:
        if fix_stack:
            top = fix_stack[-1]
            pre_text = top.before + top.change + top.after if top.chg_type == 1 else top.before + top.after
            before, change, after = _error_check_and_fix(pre_text, rec.before, rec.change, rec.chg_type, rec.after)
            if not change:
                continue  # skip empty change
            fix_stack.append(EditRec(before, after, change, rec.chg_type, rec.f_name, rec.uuid, rec.time))
        else:
            fix_stack.append(rec)
    return fix_stack


def merge_one_line_records(records):
    stack = deque()
    for rec in records:
        before = rec['changeTextStartBeforeText']
        after = rec['changeTextEndAfterText']
        change = rec['changeText']
        if not change:
            continue  # skip empty change
        chg_type = rec['changeType']
        f_name = rec['filename']
        uuid = rec['uuid']
        time = int(rec['createTime'])
        assert chg_type in {0, 1}
        if stack:
            top = stack[-1]
            if top.uuid != uuid:
                print("uuid is wrong!")
            if top.time > time:
                print("time is wrong!")
            if top.f_name != f_name:  # met new file
                stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
                continue
            if top.chg_type == 1:
                if chg_type == 1:  # insert, insert
                    if len(top.before) <= len(before) <= len(top.before) + len(top.change):  # merge
                        stack.pop()
                        change = before[len(top.before):] + change + after[:-len(top.after)]
                        stack.append(EditRec(top.before, top.after, change, chg_type, f_name, uuid, time))
                    elif len(before) < len(top.before):
                        m = top.before[len(before)-len(top.before):]
                        if not m.strip():  # only contain '\n' and space, still merge
                            stack.pop()
                            change = change + m + top.change
                            stack.append(EditRec(before, top.after, change, chg_type, f_name, uuid, time))
                        else:  # no merge
                            stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
                    elif len(after) < len(top.after):
                        m = top.after[:len(top.after)-len(after)]
                        if not m.strip():  # only contain '\n' and space, still merge
                            stack.pop()
                            change = top.change + m + change
                            stack.append(EditRec(top.before, after, change, chg_type, f_name, uuid, time))
                        else:  # no merge
                            stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
                    else:
                        stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
                else:  # insert, delete
                    if len(top.before) == len(before) and len(top.after) == len(after) and len(top.change) == len(change):  # delete exactly
                        stack.pop()  # vanish two edits
                    elif len(top.before) <= len(before) and len(top.after) <= len(after) and len(change) < len(top.change):  # delete inside
                        stack.pop()  # merge to one insert
                        change = top.change[:len(before)-len(top.before)] + (top.change[len(top.after)-len(after):] if len(top.after) < len(after) else '')
                        stack.append(EditRec(top.before, top.after, change, 1, f_name, uuid, time))
                    elif len(top.before) < len(before) < len(top.before) + len(top.change) < len(before) + len(change):  # delete overlap
                        stack.pop()  # merge to one insert and one delete
                        insert_change = top.change[:len(before)-len(top.before)]
                        stack.append(EditRec(top.before, top.after, insert_change, 1, f_name, uuid, top.time))
                        del_change = change[len(after)-len(top.after):]
                        stack.append(EditRec(before, after, del_change, 0, f_name, uuid, time))
                    elif len(before) < len(top.before) < len(before) + len(change) < len(top.before) + len(top.change):  # delete overlap
                        stack.pop()  # merge to one insert and one delete
                        insert_change = top.change[len(top.after)-len(after):]
                        stack.append(EditRec(top.before, top.after, insert_change, 1, f_name, uuid, top.time))
                        del_change = change[:len(top.before)-len(before)]
                        stack.append(EditRec(before, after, del_change, 0, f_name, uuid, time))
                    elif len(before) <= len(top.before) and len(after) <= len(top.after) and len(change) > len(top.change):  # delete cover
                        stack.pop()  # merge to one delete
                        del_change = change[:len(top.before)-len(before)] + change[len(after)-len(top.after):]
                        stack.append(EditRec(before, after, del_change, 0, f_name, uuid, time))
                    else:  # no merge
                        stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
            else:
                if chg_type == 0:  # delete, delete
                    if len(top.before) == len(before):  # merge after
                        stack.pop()
                        after = top.after[len(change):]
                        change = top.change + change
                        stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
                    elif len(top.after) == len(after):  # merge before
                        stack.pop()
                        before = top.before[:-len(change)]
                        change = change + top.change
                        stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
                    else:
                        stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
                else:  # delete, insert
                    if len(top.before) == len(before) and len(top.after) == len(after) and top.change == change:  # insert exactly
                        stack.pop()  # vanish two edits
                    else:  # no merge
                        stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
        else:
            stack.append(EditRec(before, after, change, chg_type, f_name, uuid, time))
    return error_check_and_fix(stack)


def merge_and_update(top, add):
    assert top is not None
    assert add is not None
    assert top.can_merge
    before = add.before
    after = add.after
    change = add.change
    chg_type = add.chg_type
    time = add.time
    if top.chg_type == 1:
        if chg_type == 1:  # insert, insert
            if len(top.before) <= len(before) <= len(top.before) + len(top.change):  # merge
                mer_change = top.change[:len(before)-len(top.before)] + change + top.change[len(before)-len(top.before):]
                mer_top = EditStep2(top.before, top.after, mer_change, 1, time, True, top.before, top.after, time)
                return mer_top, add, True
            elif len(before) < len(top.before):
                # no merge, only update
                mod_before = top.before[:len(before)] + change + top.before[len(before):]
                mer_top = EditStep2(top.before, top.after, top.change, 1, top.time, True, mod_before, top.after, time)
                return mer_top, add, False
            elif len(after) < len(top.after):
                # no merge, only update
                mod_after = top.after[:len(top.after)-len(after)] + change + top.after[-len(after):]
                mer_top = EditStep2(top.before, top.after, top.change, 1, top.time, True, top.before, mod_after, time)
                return mer_top, add, False
            else:
                return top, add, False
        else:  # insert, delete
            if len(top.before) == len(before) and len(top.after) == len(after) and len(top.change) == len(change):  # delete exactly
                return None, add, True  # vanish two edits
            elif len(top.before) <= len(before) and len(before) + len(change) <= len(top.before) + len(top.change):  # delete inside
                # merge to one insert
                change = top.change[:len(before) - len(top.before)] + top.change[len(top.after) - len(after):]
                mer_top = EditStep2(top.before, top.after, change, 1, time, True, top.before, top.after, time)
                return mer_top, add, True
            elif len(top.before) < len(before) < len(top.before) + len(top.change) < len(before) + len(change):  # delete overlap
                # merge to one insert and one delete
                insert_change = top.change[:len(before) - len(top.before)]
                del_change = change[len(after) - len(top.after):]
                mer_top = EditStep2(top.before, top.after, insert_change, 1, time, True, top.before, top.after, time)
                # mer_add = EditRec(before, after, del_change, 0, add.f_name, add.uuid, time)
                return mer_top, add, False
            elif len(before) < len(top.before) < len(before) + len(change) < len(top.before) + len(top.change):  # delete overlap
                # merge to one insert and one delete
                insert_change = top.change[len(top.after) - len(after):]
                del_change = change[:len(top.before) - len(before)]
                mer_top = EditStep2(top.before, top.after, insert_change, 1, time, True, top.before, top.after, time)
                # mer_add = EditRec(before, after, del_change, 0, add.f_name, add.uuid, time)
                return mer_top, add, False
            elif len(before) <= len(top.before) and len(after) <= len(top.after) and len(change) > len(top.change):  # delete cover
                # merge to one delete
                del_change = change[:len(top.before) - len(before)] + change[len(after) - len(top.after):]
                mer_top = EditStep2(before, after, del_change, 0, time, True, before, after, time)
                return mer_top, add, True
            else:  # no merge, only update
                mod_before = top.before[:len(before)] + top.before[len(before)+len(change):] if len(before) < len(
                    top.before) else top.before
                mod_after = top.after[:-len(change)-len(after)] + top.after[-len(after):] if len(after) < len(
                    top.after) else top.after
                mer_top = EditStep2(top.before, top.after, top.change, 1, top.time, True, mod_before, mod_after, time)
                return mer_top, add, False
    else:
        if chg_type == 0:  # delete, delete
            if len(top.before) == len(before):  # merge after
                after = top.after[len(change):]
                change = top.change + change
                mer_top = EditStep2(before, after, change, 0, time, True, before, after, time)
                return mer_top, add, True
            elif len(top.after) == len(after):  # merge before
                before = top.before[:-len(change)]
                change = change + top.change
                mer_top = EditStep2(before, after, change, 0, time, True, before, after, time)
                return mer_top, add, True
            else:  # no merge, only update
                mod_before = top.before[:len(before)] + top.before[len(before) + len(change):] if len(before) < len(
                    top.before) else top.before
                mod_after = top.after[:-len(change) - len(after)] + top.after[-len(after):] if len(after) < len(
                    top.after) else top.after
                mer_top = EditStep2(top.before, top.after, top.change, 0, top.time, True, mod_before, mod_after, time)
                return mer_top, add, False
        else:  # delete, insert
            if len(top.before) == len(before) and len(top.after) == len(after) and top.change == change:  # insert exactly
                return None, add, True  # vanish two edits
            else:  # no merge, only update
                mod_before = top.before[:len(before)] + change + top.before[len(before):] if len(before) < len(
                    top.before) else top.before
                mod_after = top.after[:len(top.after) - len(after)] + change + top.after[-len(after):] if len(
                    after) < len(top.after) else top.after
                if len(top.before) <= len(before) <= len(top.before) + len(top.change):
                    can_merge = False
                elif len(before) < len(top.before) < len(before) + len(change):
                    can_merge = False
                else:
                    can_merge = True
                mer_top = EditStep2(top.before, top.after, top.change, 0, top.time, can_merge, mod_before, mod_after, time)
                return mer_top, add, False


def merge_one_line_step2(records):
    merge_recs = {}
    for rec in records:
        if rec.f_name in merge_recs:
            new_merged = []
            tag = False
            add = rec
            for pre in merge_recs[rec.f_name]:
                if pre.can_merge:
                    mer_g, add, merged_one = merge_and_update(pre, add)
                    tag = True if merged_one else tag
                    if mer_g:
                        new_merged.append(mer_g)
                else:
                    new_merged.append(pre)
            if not tag and len(add.change) > 0:
                new_merged.append(EditStep2(add.before, add.after, add.change, add.chg_type, add.time, True, add.before,
                                            add.after, add.time))
            merge_recs[rec.f_name] = new_merged
        else:
            merge_recs[rec.f_name] = []
            merge_recs[rec.f_name].append(EditStep2(rec.before, rec.after, rec.change, rec.chg_type, rec.time,
                                                    True, rec.before, rec.after, rec.time))
    return merge_recs


def get_difference_of_one_line(records):
    rets = {}
    for rec in records:
        before = rec['changeTextStartBeforeText']
        after = rec['changeTextEndAfterText']
        change = rec['changeText']
        chg_type = rec['changeType']
        f_name = rec['filename']
        assert chg_type in {0, 1}
        if f_name in rets:
            if chg_type == 1:
                text = before + change + after
            else:
                text = before + after
            rets[f_name][1] = text
        else:
            if chg_type == 1:
                text = before + after
            else:
                text = before + change + after
            rets[f_name] = [text, ""]
    return rets


def merge_examples(path, begin, count, save_folder):
    assert begin >= 0
    n = -1

    def get_offset(e):
        return len(e.before)

    def get_time(e):
        return e.org_time

    step1_gz_path = os.path.join(save_folder, f"merged_example_[{begin}-{begin+count})_step1.ndjson.gz")
    with gzip.open(path, 'rb') as f:
        with gzip.open(step1_gz_path, 'wb') as gzf:
            for line in f:
                line = line.decode()
                n += 1
                if n < begin:
                    continue
                if n >= begin + count:
                    break
                ss = line.split("写入日志：")[1].strip()
                dd = eval(ss)
                dd = eval(dd['json:'])
                merges_step1 = merge_one_line_records(dd)
                # merges = merge_one_line_step2(merges_step1)
                # for fname in merges:
                #     merges[fname] = sorted(merges[fname], key=get_time)

                step1_json = os.path.join(save_folder, f"merged_example_{n}_step1.json")
                step1_track_json = os.path.join(save_folder, f"merged_example_{n}_step1_track.json")
                with open(step1_json, 'w', encoding='utf-8') as wf:
                    # merges = sorted(merges, key=get_offset)
                    for t in merges_step1:
                        dt = {'start_offset': len(t.before), 'changeType': t.chg_type, 'changeLen': len(t.change),
                              'time': t.time, 'change': t.change, 'before': t.before, 'after': t.after}
                        jstr = json.dumps(dt, indent=4, ensure_ascii=False)
                        wf.write(jstr)
                        wf.write('\n')

                with open(step1_track_json, 'w', encoding='utf-8') as wf:
                    # merges = sorted(merges, key=get_offset)
                    for t in merges_step1:
                        dt = {'start_offset': len(t.before), 'changeType': t.chg_type, 'changeLen': len(t.change),
                              'time': t.time, 'change': t.change}
                        jstr = json.dumps(dt, ensure_ascii=False)
                        wf.write(jstr)
                        wf.write('\n')

                recs = []
                for t in merges_step1:
                    dt = {'before': t.before, 'after': t.after, 'change': t.change, 'type': t.chg_type,
                          'filename': t.f_name, 'time': t.time}
                    recs.append(dt)
                line_dt = {'merges': recs}
                jstr = json.dumps(line_dt, ensure_ascii=False) + '\n'
                gzf.write(jstr.encode(encoding='utf-8'))

            '''
            i = 0
            for fname in merges:
                mg_json_path = os.path.join(save_folder, f"merged_example_{n}_{i}.json")
                trk_json_path = os.path.join(save_folder, f"merged_example_{n}_{i}_track.json")
                i += 1
                with open(mg_json_path, 'w', encoding='utf-8') as wf:
                    for t in merges[fname]:
                        dt = {'changeTextStartBeforeText': t.org_before, 'changeTextEndAfterText': t.org_after,
                              'changeText': t.change, 'changeType': t.chg_type, 'filename': fname,
                              'createTime': t.org_time}
                        jstr = json.dumps(dt, indent=4, ensure_ascii=False)
                        wf.write(jstr)
                        wf.write('\n')

                with open(trk_json_path, 'w', encoding='utf-8') as wf:
                    # merges = sorted(merges, key=get_offset)
                    for t in merges[fname]:
                        dt = {'start_offset': len(t.org_before), 'changeType': t.chg_type, 'changeLen': len(t.change),
                              'time': t.org_time, 'change': t.change}
                        jstr = json.dumps(dt, ensure_ascii=False)
                        wf.write(jstr)
                        wf.write('\n')
            '''


def merge_gz_log(log_path, save_dir):
    gz_name = os.path.basename(log_path)[:-6] + 'ndjson.gz'
    save_gz_path = os.path.join(save_dir, gz_name)
    with gzip.open(log_path, 'rb') as f:
        with gzip.open(save_gz_path, 'wb') as gzf:
            for line in f:
                line = line.decode()
                ss = line.split("写入日志：")[1].strip()
                dd = eval(ss)
                dd = eval(dd['json:'])
                merges_step1 = merge_one_line_records(dd)
                recs = []
                for t in merges_step1:
                    dt = {'before': t.before, 'after': t.after, 'change': t.change, 'type': t.chg_type,
                          'filename': t.f_name, 'time': t.time}
                    recs.append(dt)
                line_dt = {'merges': recs}
                jstr = json.dumps(line_dt, ensure_ascii=False) + '\n'
                gzf.write(jstr.encode(encoding='utf-8'))


def diff_examples(path, begin, count, save_folder):
    assert begin >= 0
    n = -1

    with gzip.open(path, 'rb') as f:
        for line in f:
            line = line.decode()
            n += 1
            if n < begin:
                continue
            if n > count-1:
                break
            ss = line.split("写入日志：")[1].strip()
            dd = eval(ss)
            dd = eval(dd['json:'])
            diffs = get_difference_of_one_line(dd)
            i = 0
            for ds in diffs.values():
                org_fp = os.path.join(save_folder, f"example_{n}_{i}_org.txt")
                mod_fp = os.path.join(save_folder, f"example_{n}_{i}_mod.txt")
                i += 1
                with open(org_fp, 'w', encoding='utf-8') as wf:
                    wf.write(ds[0])
                with open(mod_fp, 'w', encoding='utf-8') as wf:
                    wf.write(ds[1])
